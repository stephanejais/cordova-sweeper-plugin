Custom Keyboards For iOS
======

Description
-------------------
Use this plugin to remove cookies and local storage on iOS.


Installation
-------
~~~
cordova plugin add https://stephanejais@bitbucket.org/stephanejais/cordova-sweeper-plugin.git
~~~

Methods
-------

- SweeperPlugin.clearCookies()
- SweeperPlugin.clearLocalStorage()
- SweeperPlugin.clearLocalStorageAndCookies()


Supported Platforms
-------------------

- iOS


License
-------------------

MIT
