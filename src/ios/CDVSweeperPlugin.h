#import <Cordova/CDVPlugin.h>

@interface CDVSweeperPlugin : CDVPlugin {
}
- (void)clearCookies:(CDVInvokedUrlCommand*)command;
- (void)clearLocalStorage:(CDVInvokedUrlCommand*)command;
- (void)clearLocalStorageAndCookies:(CDVInvokedUrlCommand*)command;

@end
