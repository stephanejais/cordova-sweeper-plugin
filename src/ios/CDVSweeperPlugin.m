#import "CDVSweeperPlugin.h"

@interface CDVSweeperPlugin ()

@end

@implementation CDVSweeperPlugin

- (void)pluginInitialize
{
    
}

- (void)clearCookies:(CDVInvokedUrlCommand*)command
{
  NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
  for (NSHTTPCookie *cookie in [storage cookies]) {
     [storage deleteCookie:cookie];
  }
  [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)clearLocalStorage:(CDVInvokedUrlCommand*)command
{
  NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
  NSArray *array = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
  for (NSString *string in array) {
    NSLog(@"Removing %@", [path stringByAppendingPathComponent:string]);
    if ([[string pathExtension] isEqualToString:@"localstorage"])
        [[NSFileManager defaultManager] removeItemAtPath:[path stringByAppendingPathComponent:string] error:nil];
  }
}

- (void)clearLocalStorageAndCookies:(CDVInvokedUrlCommand*)command
{
  [self clearCookies:command];
  [self clearLocalStorage:command];
}

@end
