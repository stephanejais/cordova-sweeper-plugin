var argscheck = require('cordova/argscheck'),
    utils = require('cordova/utils'),
    exec = require('cordova/exec');
   
var SweeperPlugin = function() {
};

SweeperPlugin.clearCookies = function() {
    exec(null, null, "SweeperPlugin", "clearCookies", []);
    document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
};
SweeperPlugin.clearLocalStorage = function() {
    exec(null, null, "SweeperPlugin", "clearLocalStorage", []);
    if ("undefined" != typeof(window.localStorage))
      window.localStorage.clear();
};
SweeperPlugin.clearLocalStorageAndCookies = function() {
    SweeperPlugin.clearCookies();
    SweeperPlugin.clearLocalStorage();
};


module.exports = SweeperPlugin;
